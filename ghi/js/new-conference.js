window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const spinnerTag = document.getElementById('loading-conference-spinner');


    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option'); //needs to be href not id as value for option elements
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
    }


    // Here, add the 'd-none' class to the loading icon
    spinnerTag.classList.add("d-none")
    // Here, remove the 'd-none' class from the select tag
    selectTag.classList.remove("d-none")




    const formTag = document.getElementById('create-attendee-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();

            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const attendeeURL = 'http://localhost:8001/api/attendees/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(attendeeURL, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newAttendee = await response.json();
                console.log(newAttendee)
            }
            const successTag = document.getElementById('success-message');
            formTag.classList.add("d-none")
            successTag.classList.remove("d-none")
        });

  });
