function createCard(name, description, pictureUrl, starts, ends) {
    return `
      <div class="card text-center shadow bg-white rounded mt-3">
        <div class="card-body p-0">
          <img src="${pictureUrl}" class="card-img-top">
          <h5 class="card-title">${name}</h5>
          <p class="card-text">${description}</p>
          <div class="card-footer">
            ${(new Date(starts)).toLocaleDateString()} - ${(new Date(ends)).toLocaleDateString()}
        </div>
        </div>
      </div>
    `;
  }



window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
        let id = 0;

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                console.log(details.conference)
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const starts = details.conference.starts
                const ends = details.conference.ends
                const html = createCard(title, description, pictureUrl, starts, ends);
                // const column = document.querySelector('.col');
                // column.innerHTML += html;

                const columns = document.querySelectorAll(".col")
                const cardCol = id % 3;
                columns[cardCol].innerHTML += html;
                id++;

            }

        }

        // const conference = data.conferences[0];
        // const nameTag = document.querySelector('.card-title');
        // nameTag.innerHTML = conference.name;

        // const detailUrl = `http://localhost:8000${conference.href}`;
        // const detailResponse = await fetch(detailUrl);
        // if (detailResponse.ok) {
        //   const details = await detailResponse.json();
        //   const descriptionTag = document.querySelector('.card-text');
        //   descriptionTag.innerHTML = details.conference.description
        //   const imageTag = document.querySelector('.card-img-top')
        //   imageTag.src = details.conference.location.picture_url;
        //   console.log(details);


        // }

      }
    } catch (e) {
        console.error(e)
      // Figure out what to do if an error is raised
    }

  });
