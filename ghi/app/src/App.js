import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import { BrowserRouter } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
      <AttendeesList attendees={props.attendees} />
        <Routes>
          <Route path="locations">
            <Route path="new" element= { <LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element= { <ConferenceForm /> } />
          </Route>
          <Route path ="attendees">
            <Route path="new" element= { <AttendConferenceForm /> } />
          </Route>
        </Routes>
        {/* <ConferenceForm /> */}
        {/* <LocationForm /> */}

      </div>
    </BrowserRouter>
  );
}

export default App;
